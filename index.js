const puppeteer = require('puppeteer');
const moment = require('moment');
const mailer = require('./mailer.js');

const autoScroll = async (page) => {
  await page.evaluate(async () => {
    await new Promise((resolve, reject) => {
      let totalHeight = 0
      let distance = 100
      let timer = setInterval(() => {
        let scrollHeight = document.body.scrollHeight
        window.scrollBy(0, distance)
        totalHeight += distance
        if(totalHeight >= scrollHeight){
          clearInterval(timer)
          resolve()
        }
      }, 100)
    })
  })
}

async function run() {

  const args = process.argv.slice(2);

  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  
  let url = args[0];
  let datetime = moment().format('YYYY-MM-DD HH-mm');
  let filename = url.split('//')[1].replace(/\//g, ' - ') + ' ' + datetime + '.png';

  await page.setViewport({ width: 1280, height: 1024 })
  await page.goto(url);
  await page.waitFor(10*1000);
  await autoScroll;
  await page.waitFor(10*1000);

  await page.screenshot({ path: 'screenshots/' + filename, fullPage: true });
  
  browser.close();

  mailer(filename);
}

run();
