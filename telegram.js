const creds = require('./creds.js');
const messages = require('./messages.json');

var TelegramBot = require('node-telegram-bot-api');
var token = creds.telegram.token;
var bot = new TelegramBot(token, {polling: true});

bot.onText(/echo (.+)/, function (msg, match) {
    //var fromId = msg.from.id;
    var resp = match[1];
    bot.sendMessage(msg.chat.id, resp);
});

bot.on('message', function (msg) {
    var chatId = msg.chat.id;
//    // Фотография может быть: путь к файлу, поток(stream) или параметр file_id
//    var photo = 'cats.png';
//    bot.sendPhoto(chatId, photo, {caption: 'Милые котята'});
    console.log(msg);
    if (msg.text === 'uname') {
      bot.sendMessage(chatId, 'vktelebot');
    } if (msg.text.indexOf('Вова') !== -1) {
      bot.sendMessage(chatId, 'Владимир Сергеевич занят');
    }else {
//      bot.sendMessage(chatId, JSON.stringify(msg));
//      bot.sendMessage(chatId, 'Привет');
    }
});

const isNotNight = () => {
  const hours = new Date().getHours();
  return hours > 9 && hours < 22;
};

const getRandomInt = (min, max) => Math.floor(Math.random() * (max - min + 1)) + min;

//bot.sendMessage(creds.telegram.group.id, 'vktelebot started');//group
bot.sendMessage(creds.telegram.me.id, 'vktelebot started');//я
bot.sendMessage(creds.telegram.wife.id, creds.telegram.wife.name + ', Я тебя люблю!');
setInterval(() => {
  if (isNotNight()) {
    const randomIndex = getRandomInt(0, messages.messages.length - 1);
    const message = messages.messages[randomIndex];
    bot.sendMessage(creds.telegram.wife.id, creds.telegram.wife.name + '! ' + message);
  }
}, 6 * 60 * 60 * 1000);
