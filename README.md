# README #

Send page screenshot to your email

### How to run ###


Add file creds.js with your credentials

	'use strict'
	
	module.exports = {
	  sendmail: {
	    host: 'smtp.gmail.com',
 	     port: 465,
 	     secure: true, // true for 465, false for other ports
 	     auth: {
 	       user: 'test1@domain.com', 
 	       pass: '******' 
	      }
	  },
	  receivemail: [
	    'test2@domain.com',
	    'test3@domain.com'
	  ]
	}

execute

	npm install
	node index.js <url>

for example

	node index.js https://github.com/GoogleChrome/puppeteer

