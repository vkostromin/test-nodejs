'use strict';
const nodemailer = require('nodemailer');
const creds = require('./creds.js');

module.exports = function (filename) {
  // create reusable transporter object using the default SMTP transport
  let transporter = nodemailer.createTransport(creds.sendmail);

  // setup email data with unicode symbols
  let mailOptions = {
    from: '"Tester" <' + creds.sendmail.auth.user + '>', // sender address
    to: creds.receivemail.join(', '), // list of receivers
    subject: filename + ' ✔', // Subject line
    text: 'Hello world?', // plain text body
    html: '<b>Hello world?</b><img src="cid:unique"/>',
    attachments: [{
      filename: filename,
      path: 'screenshots/' + filename,
      cid: 'unique' //same cid value as in the html img src
    }]
  };

  // send mail with defined transport object
  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      return console.log(error);
    }
    console.log('Message sent: %s', info.messageId);
  });
};
